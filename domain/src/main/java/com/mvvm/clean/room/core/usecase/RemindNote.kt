package com.mvvm.clean.room.core.usecase

import com.core.data.data.Note
import com.mvvm.clean.room.core.repository.NotesReminderRepository

class RemindNote(private val reminderRepository : NotesReminderRepository) {
    fun remind(note : Note, remindAtTimeInMillis : Long) {
        reminderRepository.remindAt(note, remindAtTimeInMillis)
    }
}