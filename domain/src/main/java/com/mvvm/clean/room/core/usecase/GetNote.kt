package com.mvvm.clean.room.core.usecase

import com.core.data.data.Note
import com.core.data.repository.NoteRepository

class GetNote(private val noteRepository: NoteRepository) {
    suspend operator fun invoke(id : Long): Note? {
        return noteRepository.getNote(id)
    }
}