package com.mvvm.clean.room.core.contract.base

interface IController<T, K : IController.IControllerListener<T>> {
    interface IControllerListener<T> {
        fun onDataLoaded(data : T)
    }

    fun loadData()

    fun register(listener : K)
    fun unRegister(listener: K)
}
