package com.mvvm.clean.room.core.contract.detail

import com.core.data.data.Note
import com.mvvm.clean.room.core.contract.base.IController
import com.mvvm.clean.room.core.contract.detail.IDetailViewModelController.IDetailViewModelControllerListener

interface IDetailViewModelController :
    IController<Note, IDetailViewModelControllerListener> {
    interface IDetailViewModelControllerListener : IController.IControllerListener<Note> {
        fun onNoteDeleted()
        fun onNoteSaved()
        fun onError(message : String)
    }

    fun saveNote(currentNode: Note)
    fun deleteNote(currentNode: Note)
}
