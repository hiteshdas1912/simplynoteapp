package com.mvvm.clean.room.core.usecase

import com.core.data.data.Note
import com.core.data.repository.NoteRepository

class RemoveNote(private val noteRepository: NoteRepository) {
    suspend operator fun invoke(note : Note) {
        noteRepository.removeNote(note)
    }
}