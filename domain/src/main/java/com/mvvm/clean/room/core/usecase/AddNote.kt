package com.mvvm.clean.room.core.usecase

import com.core.data.data.Note
import com.core.data.repository.NoteRepository

class AddNote(private val nodeRepository: NoteRepository) {
    suspend operator fun invoke(note : Note) {
        nodeRepository.addNote(note)
    }
}