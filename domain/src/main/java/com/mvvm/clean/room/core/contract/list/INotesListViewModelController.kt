package com.mvvm.clean.room.core.contract.list

import com.core.data.data.Note
import com.mvvm.clean.room.core.contract.base.IController

import com.mvvm.clean.room.core.contract.list.INotesListViewModelController.INotesListViewModelControllerListener

interface INotesListViewModelController :
    IController<List<Note>, INotesListViewModelControllerListener> {

    interface INotesListViewModelControllerListener : IController.IControllerListener<List<Note>> {
        fun onNoteDeleted(deleted: Boolean)
    }

    fun remindAt(note: Note, remindAt : Long)
    fun deleteNote(note: Note)
    fun saveNote(source: String, spokenText: String)
}
