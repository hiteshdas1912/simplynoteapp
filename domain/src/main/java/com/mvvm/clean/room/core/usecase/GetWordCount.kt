package com.mvvm.clean.room.core.usecase

import com.core.data.data.Note

class GetWordCount() {
    operator fun invoke(note: Note) = getCount(note.content)
    private fun getCount(str: String) =
        str.length
}