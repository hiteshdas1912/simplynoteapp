package com.mvvm.clean.room.core.usecase

import com.core.data.data.Note
import com.core.data.repository.NoteRepository

class GetAllNotes(private val noteRepository: NoteRepository) {
    suspend operator fun invoke(): List<Note> {
        return noteRepository.getAllNotes()
    }
}