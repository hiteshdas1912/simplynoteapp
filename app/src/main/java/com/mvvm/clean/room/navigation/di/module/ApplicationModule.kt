package com.mvvm.clean.room.navigation.di.module

import android.app.Application
import com.core.data.LogService
import com.mvvm.clean.room.navigation.ProjectLog
import com.mvvm.clean.room.navigation.ui.navigation.NotesScreenNavigator
import dagger.Module
import dagger.Provides
import olx.com.presentation.navigation.NavigationInterface

@Module
class ApplicationModule(val app : Application) {
    @Provides
    fun providesApp() = app

    @Provides
    fun provideScreenNavigator(navigator : NotesScreenNavigator) : NavigationInterface {
        return navigator
    }

    @Provides
    fun provideLogService(logService: ProjectLog) : LogService {
        return logService
    }
}