package com.mvvm.clean.room.navigation.ui.view.controller

import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.core.data.data.Note
import com.mvvm.clean.room.core.contract.list.INotesListViewModelController
import com.mvvm.clean.room.core.contract.list.INotesListViewModelController.INotesListViewModelControllerListener
import com.mvvm.clean.room.navigation.ui.view.NotesListFragment
import com.mvvm.clean.room.navigation.ui.viewmodel.NotesListViewModel
import javax.inject.Inject

class NotesListController @Inject constructor() : INotesListViewModelController {

    private lateinit var viewModel: NotesListViewModel

    override fun remindAt(note: Note, remindAt: Long) {
        viewModel.remind(note, remindAt)
    }

    override fun deleteNote(note: Note) {
        viewModel.deleteNote(note)
    }

    override fun saveNote(source : String, spokenText: String) {
        viewModel.saveNote(Note(source, spokenText, System.currentTimeMillis(), System.currentTimeMillis(), 0, 0))
    }

    override fun loadData() {
        viewModel.getAllNotes()
    }

    override fun register(listener: INotesListViewModelControllerListener) {
        viewModel = ViewModelProviders.of(listener as NotesListFragment).get(NotesListViewModel::class.java)

        viewModel.getAllNotesLiveData().observe(listener, Observer {
            listener.onDataLoaded(it)
        })

        viewModel.getDeleteNotesLiveData().observe(listener, Observer {
            if (it) {
                listener.onNoteDeleted(it)
                viewModel.getDeleteNotesLiveData().postValue(false)
            }
        })
    }

    override fun unRegister(listener: INotesListViewModelControllerListener) {

    }
}