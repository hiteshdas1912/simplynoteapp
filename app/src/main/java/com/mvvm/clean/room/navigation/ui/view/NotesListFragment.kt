package com.mvvm.clean.room.navigation.ui.view

import android.app.Activity
import android.app.AlertDialog
import android.app.DatePickerDialog
import android.app.DatePickerDialog.OnDateSetListener
import android.app.TimePickerDialog
import android.app.TimePickerDialog.OnTimeSetListener
import android.content.Intent
import android.graphics.Bitmap
import android.hardware.Camera
import android.os.Bundle
import android.provider.MediaStore
import android.speech.RecognizerIntent
import android.text.TextUtils
import android.view.Gravity
import android.view.View
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.TextView
import android.widget.Toast
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.core.data.data.Note
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.mvvm.clean.room.core.contract.list.INotesListViewModelController
import com.mvvm.clean.room.core.contract.list.INotesListViewModelController.INotesListViewModelControllerListener
import com.mvvm.clean.room.navigation.FontHelper
import com.mvvm.clean.room.navigation.R
import com.mvvm.clean.room.navigation.TextRecognitionProcessor
import com.mvvm.clean.room.navigation.di.component.DaggerNotesViewComponent
import com.mvvm.clean.room.navigation.ui.adapter.NoteListAdapter
import com.mvvm.clean.room.navigation.ui.view.NotesListFragment.ListFragmentViewHolder
import com.mvvm.clean.room.navigation.ui.view.actions.ListAction
import olx.com.presentation.BaseFragment
import java.util.*
import javax.inject.Inject


class NotesListFragment : BaseFragment<ListFragmentViewHolder>(),
    INotesListViewModelControllerListener, TextRecognitionProcessor.ImageProcessorCallback {

    private val TAKE_PICTURE_REQUEST_CODE = 1001
    @Inject
    lateinit var notesListController: INotesListViewModelController

    inner class ListFragmentViewHolder(containerView: View) :
        ListAction {
        private val loadingView: ProgressBar = getViewById(containerView, R.id.loadingView)
        private val notesListView: RecyclerView = getViewById(containerView, R.id.notesListView)
        private val emptyView: ConstraintLayout = getViewById(containerView, R.id.emptyView)
        private val emptyViewTitle: TextView = getViewById(containerView, R.id.emptyViewText)
        private val app_title: TextView = getViewById(containerView, R.id.app_title)
        private val addNote: FloatingActionButton = getViewById(containerView, R.id.addNote)
        private val voiceNote: ImageView = getViewById(containerView, R.id.voiceNote)
        private val scanNote: ImageView = getViewById(containerView, R.id.scanNote)

        private var noteListAdapter: NoteListAdapter =
            NoteListAdapter(this)

        init {
            app_title.typeface = FontHelper.custom_font
            emptyViewTitle.typeface = FontHelper.custom_font
            notesListView.adapter = noteListAdapter
            notesListView.layoutManager = StaggeredGridLayoutManager(2, RecyclerView.VERTICAL)
            addNote.setOnClickListener {
                gotoNoteDetail()
            }

            voiceNote.setOnClickListener {
                displaySpeechRecognizer()
            }

            scanNote.setOnClickListener {
                startScanner()
            }

            val numCameras = Camera.getNumberOfCameras();
            val hasCamera = numCameras > 0
            if (hasCamera) {
                scanNote.visibility = View.VISIBLE
            } else {
                scanNote.visibility = View.GONE
            }
        }

        fun bindView(list: List<Note>?) {
            loadingView.visibility = View.GONE
            if (list.isNullOrEmpty()) {
                emptyView.visibility = View.VISIBLE
            } else {
                emptyView.visibility = View.GONE
            }
            noteListAdapter.setNotes(list)
        }

        override fun onListItemClicked(id: Long) {
            gotoNoteDetail(id)
        }

        override fun onRemindIconClicked(note: Note) {
            openDateAndTimePicker(note)
        }

        override fun onItemLongClicked(note: Note) {
            shareNote(note)
        }

        override fun onItemDeleted(note: Note) {
            deleteNote(note)
        }
    }

    private fun startScanner() {
        val toast= Toast.makeText(requireContext(), "Focus camera on the text you want to note", Toast.LENGTH_LONG);
        toast.setGravity(Gravity.CENTER, 0, 0)
        toast.show()
        val takePictureIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(requireActivity().packageManager) != null) {
            startActivityForResult(takePictureIntent, TAKE_PICTURE_REQUEST_CODE);
        }
    }

    private fun deleteNote(note: Note) {
        AlertDialog.Builder(context)
            .setTitle("Delete Note")
            .setMessage("Your note will be deleted permanently. Delete now?")
            .setPositiveButton("Yes") { dialog, which ->
                notesListController.deleteNote(note)
                dialog.dismiss()
            }
            .setNegativeButton("No") { dialog, which ->
                dialog.dismiss()
            }.create().show()
    }

    private fun shareNote(note: Note) {
        val shareIntent = Intent()
        shareIntent.action = Intent.ACTION_SEND
        shareIntent.type = "text/plain"
        shareIntent.putExtra(
            Intent.EXTRA_TEXT,
            "Note: \nTitle : " + note.title + "\n" + note.content + "\n - via Simply Note App https://bit.ly/2LeXkg2 (install now)"
        );
        startActivity(Intent.createChooser(shareIntent, "Share Note : " + note.title))
    }

    private fun openDateAndTimePicker(note: Note) {
        showDatePicker(note)
    }

    private fun showDatePicker(note: Note) {
        var mYear = Calendar.getInstance().get(Calendar.YEAR)
        var mMonth = Calendar.getInstance().get(Calendar.MONTH) + 1
        var mDay = Calendar.getInstance().get(Calendar.DAY_OF_MONTH)

        val mYearParam = mYear
        val mMonthParam = mMonth - 1
        val mDayParam = mDay

        val datePickerDialog = DatePickerDialog(
            requireActivity(),
            OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                mMonth = monthOfYear + 1
                mYear = year
                mDay = dayOfMonth

                showTimePicker(note, mMonth, mYear, mDay)
            }, mYearParam, mMonthParam, mDayParam
        )

        datePickerDialog.show()
    }

    private fun showTimePicker(note: Note, mMonth: Int, mYear: Int, mDay: Int) {
        var mHour = Calendar.getInstance().get(Calendar.HOUR_OF_DAY)
        var mMinute = Calendar.getInstance().get(Calendar.MINUTE)
        val timePickerDialog = TimePickerDialog(
            context,
            OnTimeSetListener { view, pHour, pMinute ->
                mHour = pHour
                mMinute = pMinute

                val dateForAlarmWithTime = Calendar.getInstance()
                dateForAlarmWithTime.set(mYear, mMonth - 1, mDay, mHour, mMinute, 0)
                notesListController.remindAt(note, dateForAlarmWithTime.time.time)
            }, mHour, mMinute, false
        )

        timePickerDialog.show()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        DaggerNotesViewComponent.builder()
            .build()
            .inject(this)
    }

    override fun onResume() {
        super.onResume()
        notesListController.loadData()
    }

    private fun gotoNoteDetail(id: Long = 0L) {
        val action = NotesListFragmentDirections.actionGoToNote(id)
        navigate(action)
    }

    override fun onCreateViewHolder(containerView: View): ListFragmentViewHolder {
        return ListFragmentViewHolder(containerView)
    }

    override fun getFragmentLayout() = R.layout.fragment_list

    override fun onViewHolderCreated() {
        notesListController.register(this)
    }

    override fun onNoteDeleted(deleted: Boolean) {
        if (deleted) {
            Toast.makeText(context, "Note Deleted", Toast.LENGTH_SHORT).show()
            notesListController.loadData()
        } else {
            Toast.makeText(context, "Error deleting note", Toast.LENGTH_SHORT).show()
        }
    }

    override fun onDataLoaded(data: List<Note>) {
        getViewHolder()!!.bindView(data)
    }

    // Create an intent that can start the Speech Recognizer activity
    private fun displaySpeechRecognizer() {
        try {
            val intent = Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH).apply {
                putExtra(
                    RecognizerIntent.EXTRA_LANGUAGE_MODEL,
                    RecognizerIntent.LANGUAGE_MODEL_FREE_FORM
                )
            }
            // Start the activity, the intent will be populated with the speech text
            startActivityForResult(intent, SPEECH_REQUEST_CODE)
        } catch (e :Exception) {
            Toast.makeText(requireContext(), "Not supported for your device. Please use text mode", Toast.LENGTH_SHORT).show()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == SPEECH_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            data?.apply {
                val spokenText: String? =
                    data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS).let { results ->
                        results[0]
                    }
                if (!TextUtils.isEmpty(spokenText)) {
                    notesListController.saveNote("Audio Input", spokenText!!)
                }
            }
        } else if (requestCode == TAKE_PICTURE_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            data?.apply {
                val bitmap = extras?.get("data") as Bitmap
                TextRecognitionProcessor(this@NotesListFragment).runTextRecognition(bitmap)
            }
        }
        super.onActivityResult(requestCode, resultCode, data)
    }

    override fun onSuccess(text: String) {
        notesListController.saveNote("Image Scan", text)
    }

    override fun onFailure(e: Exception) {
        Toast.makeText(context, "No Text Found", Toast.LENGTH_SHORT).show()
    }

    companion object {
        private const val SPEECH_REQUEST_CODE = 0
    }
}
