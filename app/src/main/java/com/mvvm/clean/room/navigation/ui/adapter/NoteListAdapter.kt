package com.mvvm.clean.room.navigation.ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.core.data.data.Note
import com.mvvm.clean.room.navigation.FontHelper
import com.mvvm.clean.room.navigation.R
import com.mvvm.clean.room.navigation.ui.view.actions.ListAction
import kotlinx.android.synthetic.main.note_item.view.*
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class NoteListAdapter(private val action : ListAction) : RecyclerView.Adapter<NoteListAdapter.NoteViewHolder>() {
    private var noteList = ArrayList<Note>()

    fun setNotes(notes : List<Note>?) {
        noteList.clear()
        notes?.apply {
            noteList.addAll(this)
        }
        notifyDataSetChanged()
    }

    inner class NoteViewHolder(view : View) : RecyclerView.ViewHolder(view) {
        private val noteLayout = view.noteLayout
        private val note_container = view.note_container
        private val title: TextView = view.title
        private val content = view.content
        private val updatedTime = view.date
        private val alarmView = view.alarmView
        private val shareView = view.shareView
        private val deleteView = view.deleteView

        fun bind(position: Int) {
            val note = noteList[position]
            title.text = note.title
            content.text = note.content
            content.typeface = FontHelper.custom_font
            updatedTime.text = "Last updated ${getFormattedDate(note.updateTime)}"
            updatedTime.typeface = FontHelper.custom_font
            noteLayout.setOnClickListener {
                listItemClicked(note)
            }

            shareView.setOnClickListener {
                onItemLongClicked(note)
            }

            alarmView.setOnClickListener {
                alarmItemClicked(note)
            }
            deleteView.setOnClickListener {
                alarmItemDeleted(note)
            }
        }
    }

    private fun alarmItemDeleted(note: Note) {
        action.onItemDeleted(note)
    }

    private fun onItemLongClicked(note: Note) {
        action.onItemLongClicked(note)
    }

    private fun alarmItemClicked(note: Note) {
        action.onRemindIconClicked(note)
    }

    private fun listItemClicked(note: Note) {
        action.onListItemClicked(note.id)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NoteViewHolder {
        return NoteViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.note_item, parent, false))
    }

    override fun getItemCount(): Int {
        return noteList.size
    }

    override fun onBindViewHolder(holder: NoteViewHolder, position: Int) {
        holder.bind(position)
    }

    private fun getFormattedDate(updateTime: Long): String? {
        val df = SimpleDateFormat("MMM dd YYYY, hh:mm a", Locale.ENGLISH)
        return df.format(updateTime)
    }
}
