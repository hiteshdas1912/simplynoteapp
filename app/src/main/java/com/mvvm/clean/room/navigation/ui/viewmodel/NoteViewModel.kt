package com.mvvm.clean.room.navigation.ui.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.core.data.data.Note
import com.mvvm.clean.room.core.usecase.AddNote
import com.mvvm.clean.room.core.usecase.GetNote
import com.mvvm.clean.room.core.usecase.RemoveNote
import com.mvvm.clean.room.navigation.di.module.ApplicationModule
import com.mvvm.clean.room.navigation.framework.di.component.DaggerViewModelComponent

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

class NoteViewModel(application: Application) : AndroidViewModel(application) {
    private val coroutineScope = CoroutineScope(Dispatchers.IO)

    @Inject
    lateinit var addNoteUseCase : AddNote
    @Inject
    lateinit var removeNoteUseCase : RemoveNote
    @Inject
    lateinit var noteUseCase : GetNote

    init {
        DaggerViewModelComponent.builder()
            .applicationModule(
                ApplicationModule(
                    application
                )
            )
            .build()
            .inject(this)
    }

    val saved = MutableLiveData<Boolean>()
    val currentNote = MutableLiveData<Note>()
    val removeNote = MutableLiveData<Boolean>()

    fun saveNote(note: Note) {
        coroutineScope.launch {
            addNoteUseCase(note)
            saved.postValue(true)
        }
    }

    fun getNote(id: Long) {
        coroutineScope.launch {
            currentNote.postValue(noteUseCase(id))
        }
    }

    fun deleteNote(note: Note) {
        coroutineScope.launch {
            removeNoteUseCase(note)
            removeNote.postValue(true)
        }
    }

}