package com.mvvm.clean.room.navigation.di.module

import android.app.Application
import com.app.framework.db.RoomNoteDataSource
import com.core.data.repository.NoteRepository
import dagger.Module
import dagger.Provides

@Module
class RepositoryModule {
    @Provides
    fun provideRepository(app : Application) = NoteRepository(RoomNoteDataSource(app))
}