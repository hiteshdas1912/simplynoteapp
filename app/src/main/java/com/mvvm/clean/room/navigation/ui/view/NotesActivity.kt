package com.mvvm.clean.room.navigation.presentation

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.mvvm.clean.room.navigation.R

class NotesActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_notes)
    }
}
