package com.mvvm.clean.room.navigation.ui.navigation

import android.view.View
import androidx.navigation.NavDirections
import androidx.navigation.Navigation
import olx.com.presentation.navigation.NavigationInterface
import javax.inject.Inject

class NotesScreenNavigator @Inject constructor():
    NavigationInterface {

    override fun navigateTo(view: View?, action: NavDirections) {
        try {
            view?.apply {
                Navigation.findNavController(this).navigate(action)
            }
        } catch (e : Exception) {

        }
    }
}
