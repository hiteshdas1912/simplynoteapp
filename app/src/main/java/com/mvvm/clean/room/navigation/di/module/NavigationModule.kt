package com.mvvm.clean.room.navigation.di.module

import com.mvvm.clean.room.navigation.ui.navigation.NotesScreenNavigator
import dagger.Module
import dagger.Provides
import olx.com.presentation.navigation.NavigationInterface

@Module
class NavigationModule {
    @Provides
    fun provideScreenNavigator(navigator : NotesScreenNavigator) : NavigationInterface {
        return navigator
    }
}