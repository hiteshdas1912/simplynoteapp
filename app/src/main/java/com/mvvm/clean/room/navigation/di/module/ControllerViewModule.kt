package com.mvvm.clean.room.navigation.di.module

import com.mvvm.clean.room.core.contract.detail.IDetailViewModelController
import com.mvvm.clean.room.core.contract.list.INotesListViewModelController
import com.mvvm.clean.room.navigation.ui.view.controller.NoteDetailController
import com.mvvm.clean.room.navigation.ui.view.controller.NotesListController
import dagger.Module
import dagger.Provides

@Module
class ControllerViewModule {
    @Provides
    fun provideListController(notesListController : NotesListController) : INotesListViewModelController {
        return notesListController
    }

    @Provides
    fun provideDetailController(notesDetailController : NoteDetailController) : IDetailViewModelController {
        return notesDetailController
    }
}