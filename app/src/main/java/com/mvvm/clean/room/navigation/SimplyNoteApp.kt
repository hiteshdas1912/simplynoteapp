package com.mvvm.clean.room.navigation
import androidx.multidex.MultiDexApplication
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.iid.FirebaseInstanceId

class SimplyNoteApp : MultiDexApplication() {
    override fun onCreate() {
        super.onCreate()
        initFireBase()
        FontHelper.init(this)
    }

    private fun initFireBase() {
        FirebaseInstanceId.getInstance().instanceId
            .addOnCompleteListener(OnCompleteListener { task ->
                if (!task.isSuccessful) {
                    return@OnCompleteListener
                }
            })
    }
}