package com.mvvm.clean.room.navigation.framework

import android.app.Application
import android.widget.Toast
import androidx.work.Data
import androidx.work.OneTimeWorkRequest
import androidx.work.WorkManager
import com.core.data.LogService
import com.core.data.data.Note
import com.mvvm.clean.room.core.repository.NotesReminderRepository
import com.mvvm.clean.room.navigation.notification.NotificationWorker
import java.util.concurrent.TimeUnit
import javax.inject.Inject


class NoteReminderRepositoryImpl @Inject constructor(val app: Application, val logService: LogService) :
    NotesReminderRepository {

    override fun remindAt(note: Note, remindAtTimeInMillis: Long) {
        val data = Data.Builder()
//Add parameter in Data class. just like bundle. You can also add Boolean and Number in parameter.
        data.putString("title", note.title)
        data.putInt("id", note.id.toInt())

        val diffInSecs = TimeUnit.MILLISECONDS.toSeconds(remindAtTimeInMillis - System.currentTimeMillis());
        if (diffInSecs < 0) {
            Toast.makeText(app.applicationContext, "Invalid time", Toast.LENGTH_SHORT).show()
            return
        }

        Toast.makeText(app.applicationContext, "Note reminder added", Toast.LENGTH_SHORT).show()

        logService.log("Seconds to Notify $diffInSecs")
        val oneTimeWorkRequest =
            OneTimeWorkRequest.Builder(NotificationWorker::class.java)
                .setInitialDelay(diffInSecs, TimeUnit.SECONDS)
                .setInputData(data.build())
                .addTag("" + note.id)
                .build()
        val workManager = WorkManager.getInstance(app.applicationContext)
        workManager.cancelAllWorkByTag("" + note.id)
        workManager.enqueue(oneTimeWorkRequest)
    }
}