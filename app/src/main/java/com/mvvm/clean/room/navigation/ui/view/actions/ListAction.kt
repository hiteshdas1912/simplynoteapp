package com.mvvm.clean.room.navigation.ui.view.actions

import com.core.data.data.Note

interface ListAction {
    fun onListItemClicked(id : Long)
    fun onRemindIconClicked(note : Note)
    fun onItemLongClicked(note: Note)
    fun onItemDeleted(note: Note)
}