package com.mvvm.clean.room.navigation.framework.di.component

import com.mvvm.clean.room.navigation.di.module.ApplicationModule
import com.mvvm.clean.room.navigation.di.module.RepositoryModule
import com.mvvm.clean.room.navigation.di.module.UseCaseModule
import com.mvvm.clean.room.navigation.ui.viewmodel.NoteViewModel
import com.mvvm.clean.room.navigation.ui.viewmodel.NotesListViewModel
import dagger.Component

@Component(modules = [
    ApplicationModule::class,
    RepositoryModule::class,
    UseCaseModule::class
])
interface ViewModelComponent {
    fun inject(listViewModel: NotesListViewModel)
    fun inject(detailViewModel: NoteViewModel)
}