package com.mvvm.clean.room.navigation

import android.graphics.Bitmap
import android.os.Handler
import android.os.Looper
import android.text.TextUtils
import com.google.firebase.ml.vision.FirebaseVision
import com.google.firebase.ml.vision.common.FirebaseVisionImage
import com.google.firebase.ml.vision.text.FirebaseVisionText
import java.util.concurrent.Executors


/** Processor for the text recognition demo.  */
class TextRecognitionProcessor(val callback : ImageProcessorCallback) {
    val executorService = Executors.newSingleThreadExecutor()
    val handler = Handler(Looper.getMainLooper())
    fun runTextRecognition(imageBitmap : Bitmap) {
        executorService.submit {
            val image = FirebaseVisionImage.fromBitmap(imageBitmap)
            val recognizer = FirebaseVision.getInstance()
                .onDeviceTextRecognizer
            recognizer.processImage(image)
                .addOnSuccessListener{
                    processTextRecognitionResult(it)
                }.addOnFailureListener{
                    handler.post {
                        callback.onFailure(TextNotFoundException())
                    }
                }
        }
    }

    private fun processTextRecognitionResult(
        texts: FirebaseVisionText
    ) {
        val blocks = texts.textBlocks
        if (blocks.size == 0) {
            handler.post {
                callback.onFailure(TextNotFoundException())
            }
            return
        }

        val text = StringBuilder()
        for (i in blocks.indices) {
            val lines = blocks[i].lines
            for (j in lines.indices) {
                val elements =
                    lines[j].elements
                for (k in elements.indices) {
                    val element = elements[k]
                    text.append("\n").append(element.text)
                }
            }
        }
        handler.post {
            val toString = text.toString()
            if (!TextUtils.isEmpty(toString)) {
                callback.onSuccess(toString)
            } else {
                callback.onFailure(TextNotFoundException())
            }
        }
    }

    class TextNotFoundException() : Exception() {

    }

    interface ImageProcessorCallback {
        fun onSuccess(text : String)
        fun onFailure(e : Exception)
    }

}