package com.mvvm.clean.room.navigation.notification

import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.os.Build
import androidx.core.app.NotificationCompat
import androidx.work.Worker
import androidx.work.WorkerParameters
import olx.com.presentation.R


class NotificationWorker(val context : Context, params : WorkerParameters) : Worker(context, params) {

    override fun doWork(): Result {
        triggerNotification();
        return Result.success();
    }

    private fun triggerNotification() {
        val notificationManager =
            applicationContext.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val notificationChannel = NotificationChannel(
                context.getString(R.string.simply_note_notif_channel),
                context.getString(R.string.simply_note_notif_channel),
                NotificationManager.IMPORTANCE_HIGH
            )
            notificationManager.createNotificationChannel(notificationChannel)
        }

        val notification: NotificationCompat.Builder =
            NotificationCompat.Builder(applicationContext, context.getString(R.string.simply_note_notif_channel))
                .setContentTitle(inputData.getString("title") +". You are just on time !!!")
                .setSmallIcon(android.R.drawable.ic_popup_reminder)
                .setAutoCancel(true)
        notificationManager.notify(inputData.getInt("id", 0), notification.build())
    }
}