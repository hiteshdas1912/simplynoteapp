package com.mvvm.clean.room.navigation.ui.view.controller

import androidx.lifecycle.ViewModelProviders
import com.mvvm.clean.room.core.contract.detail.IDetailViewModelController
import com.core.data.data.Note
import com.mvvm.clean.room.navigation.presentation.NoteDetailFragment
import com.mvvm.clean.room.navigation.ui.viewmodel.NoteViewModel
import javax.inject.Inject

class NoteDetailController @Inject constructor() : IDetailViewModelController {

    private var noteId: Long? = null

    fun setNoteId(noteId : Long) {
        this.noteId = noteId
    }

    private lateinit var viewModel: NoteViewModel

    override fun saveNote(currentNode: Note) {
        viewModel.saveNote(currentNode)
    }

    override fun deleteNote(currentNode: Note) {
        viewModel.deleteNote(currentNode)
    }


    override fun loadData() {
        if (noteId != null) {
            viewModel.getNote(noteId!!)
        }
    }

    override fun register(listener: IDetailViewModelController.IDetailViewModelControllerListener) {
        viewModel = ViewModelProviders.of(listener as NoteDetailFragment).get(NoteViewModel::class.java)
        viewModel.saved.observe(listener, androidx.lifecycle.Observer { saved ->
            if (saved) {
                listener.onNoteSaved()
            } else {
                listener.onError("Error saving the note")
            }
        })

        viewModel.currentNote.observe(listener, androidx.lifecycle.Observer { note ->
            note?.apply {
                listener.onDataLoaded(this)
            }
        })

        viewModel.removeNote.observe(listener, androidx.lifecycle.Observer { deleted ->
            if (deleted) {
                listener.onNoteDeleted()
            }
        })
    }

    override fun unRegister(listener: IDetailViewModelController.IDetailViewModelControllerListener) {
        // No need as we are using view models
    }
}