package com.mvvm.clean.room.navigation.di.component

import com.mvvm.clean.room.navigation.di.module.ControllerViewModule
import com.mvvm.clean.room.navigation.di.module.NavigationModule
import com.mvvm.clean.room.navigation.presentation.NoteDetailFragment
import com.mvvm.clean.room.navigation.ui.view.NotesListFragment
import dagger.Component

@Component(modules = [
    ControllerViewModule::class,
    NavigationModule::class
])
interface NotesViewComponent {
    fun inject(notesListFragment: NotesListFragment)
    fun inject(noteDetailFragment: NoteDetailFragment)
}