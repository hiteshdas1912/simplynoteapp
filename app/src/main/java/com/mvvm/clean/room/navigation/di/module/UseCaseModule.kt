package com.mvvm.clean.room.navigation.di.module

import com.core.data.repository.NoteRepository
import com.mvvm.clean.room.core.usecase.*
import com.mvvm.clean.room.navigation.framework.NoteReminderRepositoryImpl
import dagger.Module
import dagger.Provides

@Module
class UseCaseModule {

    @Provides
    fun provideAddNoteUseCase(repository: NoteRepository)= AddNote(repository)

    @Provides
    fun provideGetAllNotesUseCase(repository: NoteRepository)= GetAllNotes(repository)

    @Provides
    fun provideGetNoteUseCase(repository: NoteRepository)= GetNote(repository)

    @Provides
    fun provideRemoveNoteUseCase(repository: NoteRepository)= RemoveNote(repository)

    @Provides
    fun provideGetWordCountUseCase()= GetWordCount()

    @Provides
    fun provideRemindNoteUseCase(repository: NoteReminderRepositoryImpl)= RemindNote(repository)
}