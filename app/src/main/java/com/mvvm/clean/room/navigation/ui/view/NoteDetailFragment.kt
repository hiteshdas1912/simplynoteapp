package com.mvvm.clean.room.navigation.presentation

import android.app.AlertDialog
import android.content.Context.INPUT_METHOD_SERVICE
import android.os.Bundle
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.navigation.Navigation
import androidx.work.WorkManager
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.mvvm.clean.room.core.contract.detail.IDetailViewModelController
import com.core.data.data.Note
import com.mvvm.clean.room.navigation.FontHelper
import com.mvvm.clean.room.navigation.R
import com.mvvm.clean.room.navigation.di.component.DaggerNotesViewComponent

import com.mvvm.clean.room.navigation.ui.view.controller.NoteDetailController
import kotlinx.android.synthetic.main.fragment_note.view.*
import olx.com.presentation.BaseFragment
import javax.inject.Inject

class NoteDetailFragment : BaseFragment<NoteDetailFragment.NoteFragmentViewHolder>(),
    IDetailViewModelController.IDetailViewModelControllerListener {

    @Inject
    lateinit var noteDetailController: NoteDetailController

    private var currentNode = Note("", "", 0, 0)

    inner class NoteFragmentViewHolder(containerView: View) {
        private val titleView = getViewById<EditText>(containerView, R.id.titleView)
        private val contentView = getViewById<EditText>(containerView, R.id.contentView)

        private val deleteButton = containerView.findViewById<FloatingActionButton>(R.id.deleteButton)
        private val checkButton = containerView.findViewById<FloatingActionButton>(R.id.checkButton)
        init {
            contentView.typeface = FontHelper.custom_font

            checkButton.setOnClickListener {
                if (titleView.text.toString() != "" || contentView.text.toString() != "") {
                    val time = System.currentTimeMillis()
                    var titleText = titleView.text.toString()
                    if (titleText == "") {
                        titleText = "No Title"
                    }
                    currentNode.title = titleText
                    currentNode.content = contentView.text.toString()
                    currentNode.updateTime = time
                    if (currentNode.id == 0L) {
                        currentNode.creationTime = time
                    }
                    noteDetailController.saveNote(currentNode)
                } else {
                    Navigation.findNavController(it).popBackStack()
                }
            }

            deleteButton.setOnClickListener {
                hideKeyBoard()
                AlertDialog.Builder(context)
                    .setTitle("Delete Note")
                    .setMessage("Your note will be deleted permanently. Delete now?")
                    .setPositiveButton("Yes") { dialog, which ->
                        noteDetailController.deleteNote(currentNode)
                        dialog.dismiss()
                    }
                    .setNegativeButton("No") { dialog, which ->
                        dialog.dismiss()
                    }.create().show()
            }
        }

        fun bindCurrentNote(note: Note) {
            currentNode = note
            titleView.setText(currentNode.title, TextView.BufferType.EDITABLE)
            contentView.setText(currentNode.content, TextView.BufferType.EDITABLE)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        DaggerNotesViewComponent.builder()
            .build()
            .inject(this)
    }

    override fun getFragmentLayout() = R.layout.fragment_note

    override fun onViewHolderCreated() {
        noteDetailController.register(this)
        arguments?.let {
            val noteId = NoteDetailFragmentArgs.fromBundle(it).noteId
            if (noteId != 0L) {
                noteDetailController.setNoteId(noteId)
                noteDetailController.loadData()
            } else {
                showKeyBoard()
            }
        }
    }

    private fun showKeyBoard() {
        val inputMethodManager =
            context?.getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager
        view?.apply {
            titleView.requestFocus()
            inputMethodManager.showSoftInput(titleView, 0)
        }
    }

    override fun onCreateViewHolder(containerView: View): NoteFragmentViewHolder {
        return NoteFragmentViewHolder(containerView)
    }

    private fun navigateBack() {
        Navigation.findNavController(view!!).popBackStack()
    }

    private fun hideKeyBoard() {
        val inputMethodManager =
            context?.getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager
        view?.apply {
            inputMethodManager.hideSoftInputFromWindow(windowToken, 0)
        }
    }

    override fun onError(message : String) {
        showToast(message)
    }

    override fun onNoteDeleted() {
        hideKeyBoard()
        showToast("Note deleted")
        navigateBack()
        if (currentNode.id != 0L) {
            WorkManager.getInstance(context!!).cancelAllWorkByTag("" + currentNode.id)
        }
    }

    override fun onNoteSaved() {
        hideKeyBoard()
        showToast("Note saved")
        navigateBack() }

    private fun showToast(message: String) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
    }

    override fun onDataLoaded(data: Note) {
        getViewHolder()?.bindCurrentNote(data)
    }
}
