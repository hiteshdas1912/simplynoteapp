package com.mvvm.clean.room.navigation

import android.util.Log
import com.core.data.LogService
import javax.inject.Inject

class ProjectLog @Inject constructor(): LogService {
    override fun log(s: String) {
        if (BuildConfig.DEBUG) {
            Log.d("SimplyNote", s)
        }
    }

}
