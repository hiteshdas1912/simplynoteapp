package com.mvvm.clean.room.navigation

import android.content.Context
import android.graphics.Typeface


object FontHelper {
    var custom_font: Typeface? = null
        get() = field

    fun init(context: Context) {
        custom_font = Typeface.createFromAsset(context.getAssets(),  "fonts/Proxima_Nova_Reg.ttf")
    }
}