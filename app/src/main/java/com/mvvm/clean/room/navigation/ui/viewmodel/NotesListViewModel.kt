package com.mvvm.clean.room.navigation.ui.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.core.data.data.Note
import com.mvvm.clean.room.core.usecase.*
import com.mvvm.clean.room.navigation.di.module.ApplicationModule
import com.mvvm.clean.room.navigation.framework.di.component.DaggerViewModelComponent

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

class NotesListViewModel(application: Application): AndroidViewModel(application) {
    @Inject
    lateinit var allNotesUseCase : GetAllNotes
    @Inject
    lateinit var wordCountUseCase : GetWordCount
    @Inject
    lateinit var reminderUseCase : RemindNote

    @Inject
    lateinit var removeNote : RemoveNote
    @Inject
    lateinit var addNote : AddNote

    init {
        DaggerViewModelComponent.builder()
            .applicationModule(
                ApplicationModule(
                    application
                )
            )
            .build()
            .inject(this)
    }

    internal fun getAllNotes() {
        coroutineScope.launch {
            val notes = allNotesUseCase.invoke()
            notes.forEach {
                it.wordCount = wordCountUseCase.invoke(it)
            }
            allNotes.postValue(notes)
        }
    }

    fun remind(note: Note, remindAt: Long) {
        reminderUseCase.remind(note, remindAt)
    }

    fun getAllNotesLiveData() = allNotes
    fun getDeleteNotesLiveData() = deleteNoteData

    fun deleteNote(note: Note) {
        coroutineScope.launch {
            removeNote(note)
            deleteNoteData.postValue(true)
        }
    }

    fun saveNote(note: Note) {
        coroutineScope.launch {
            addNote(note)
            getAllNotes()
        }
    }

    companion object {
        private val coroutineScope = CoroutineScope(Dispatchers.IO)
        internal val allNotes = MutableLiveData<List<Note>>()
        internal val deleteNoteData = MutableLiveData<Boolean>()
    }
}