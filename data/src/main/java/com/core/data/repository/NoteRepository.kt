package com.core.data.repository

import com.core.data.data.Note
import com.mvvm.clean.room.core.repository.NoteDataSource

class NoteRepository(private val dataSource: NoteDataSource) {
    suspend fun addNote(note : Note) {
        dataSource.add(note)
    }
    suspend fun getNote(id : Long) : Note? {
        return dataSource.get(id)
    }
    suspend fun getAllNotes() : List<Note> {
        return dataSource.getAll()
    }
    suspend fun removeNote(note : Note) {
        return dataSource.remove(note)
    }
}