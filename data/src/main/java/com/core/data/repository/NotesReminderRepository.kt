package com.mvvm.clean.room.core.repository

import com.core.data.data.Note

interface NotesReminderRepository {
    fun remindAt(note: Note, remindAtTimeInMillis : Long)
}