package com.app.framework.db

import android.app.Application
import com.core.data.data.Note
import com.mvvm.clean.room.core.repository.NoteDataSource
import com.mvvm.clean.room.navigation.framework.db.DatabaseService
import com.mvvm.clean.room.navigation.framework.db.NoteEntity

class RoomNoteDataSource(val app: Application) : NoteDataSource {
    override suspend fun add(note: Note) {
        DatabaseService.getInstance(app).noteDao().addNote(NoteEntity.fromNote(note))
    }

    override suspend fun get(id: Long): Note? {
        return DatabaseService.getInstance(app).noteDao().getNote(id)?.toNote()
    }

    override suspend fun getAll(): List<Note> {
        return DatabaseService.getInstance(app).noteDao().getAllNoteEntities().map {
            it.toNote()
        }
    }

    override suspend fun remove(note: Note) {
        DatabaseService.getInstance(app).noteDao().deleteNoteEntity(NoteEntity.fromNote(note))
    }

}