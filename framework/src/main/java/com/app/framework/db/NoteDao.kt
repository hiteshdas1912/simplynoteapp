package com.mvvm.clean.room.navigation.framework.db

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy.REPLACE
import androidx.room.Query

@Dao
interface NoteDao {
    @Insert(onConflict = REPLACE)
    suspend fun addNote(noteEntity : NoteEntity)

    @Query("SELECT * FROM note where id = :id")
    suspend fun getNote(id : Long) : NoteEntity?

    @Query("SELECT * FROM note ORDER BY update_time DESC")
    suspend fun getAllNoteEntities() : List<NoteEntity>

    @Delete
    suspend fun deleteNoteEntity(note : NoteEntity)
}