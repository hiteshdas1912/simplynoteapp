package olx.com.presentation

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.NavDirections
import olx.com.presentation.navigation.NavigationInterface
import javax.inject.Inject

abstract class BaseFragment<ViewHolderType> : Fragment() {
    private var viewHolder: ViewHolderType? = null

    @Inject
    lateinit var screenNavigator : NavigationInterface

    open fun getViewHolder() = viewHolder

    final override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val containerView = getContainerView(inflater, container)
        viewHolder = onCreateViewHolder(containerView)
        return containerView
    }

    private fun getContainerView(
        inflater: LayoutInflater,
        container: ViewGroup?
    ) = inflater.inflate(getFragmentLayout(), container, false)

    final override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        onViewHolderCreated()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        viewHolder = null
    }

    fun navigate(action: NavDirections) {
        screenNavigator.navigateTo(view, action)
    }

    fun <T : View> getViewById(containerView: View, viewId: Int): T {
        return containerView.findViewById(viewId)
    }

    abstract fun onCreateViewHolder(containerView: View): ViewHolderType

    abstract fun getFragmentLayout(): Int

    abstract fun onViewHolderCreated()
}