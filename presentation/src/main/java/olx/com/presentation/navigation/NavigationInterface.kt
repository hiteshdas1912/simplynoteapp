package olx.com.presentation.navigation

import android.view.View
import androidx.navigation.NavDirections

interface NavigationInterface {
    fun navigateTo(view: View?, action: NavDirections)
}
